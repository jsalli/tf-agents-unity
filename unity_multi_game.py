import numpy as np
from mlagents.envs import UnityEnvironment
import time
import atexit

class UnityMultiGame:
    def __init__(self, worker_id, game_env_path):
        self._id = worker_id
        # self._env_name = "./env/32_arenas/artificial_invaders_32_areas.x86_64"
        self._env_name = game_env_path
        self._train_mode = True
        self._env = UnityEnvironment(file_name=self._env_name, worker_id=worker_id, seed=1)
        self._default_brain = self._env.brain_names[0]
        self._brain = self._env.brains[self._default_brain]
        self._state = None
        atexit.register(self.close)


    def init_game(self):
        self._state = self._env.reset(train_mode=self._train_mode)[self._default_brain]
        self._agent_ids = self._state.agents
        self._number_of_agents = len(self._state.agents)
        self._game_finishedes = [False] * self._number_of_agents
        return self._number_of_agents
    
    def close(self):
        self._env.close()

    def get_num_of_actions(self):
        return self._brain.vector_action_space_size[0]
    
    def get_num_of_observations(self):
        observations = self.get_observations()
        shape = observations.shape
        return shape[1]

    def reset(self, env_reset_parameters=None):
        if env_reset_parameters is not None:
            print("========== NEW LEVEL =====: {}".format(self._id))
        
        self._state = self._env.reset(train_mode=self._train_mode, config=env_reset_parameters)[self._default_brain]
        self._game_finishedes = [False] * self._number_of_agents

    def are_episodes_finished(self):
        return self._game_finishedes

    def make_action(self, actions):
        old_agent_order = self._agent_ids
        self._state = self._env.step(actions)[self._default_brain]
        if self._agent_ids is not old_agent_order:
            raise ValueError(
                "Agent id order has been changed from what it was in init_game "
                "Old agent id order: {} vs. New agent order {}".format(old_agent_order, self._agent_ids))

        rewards = self._state.rewards
        self._game_finishedes = self._state.local_done
        return rewards
    
    def get_observations(self):
        return self._state.vector_observations
