import numpy as np
from mlagents.envs import UnityEnvironment
import time
import atexit

class UnityGame:
    def __init__(self, worker_id):
        self._env_name = "./env/1_area/ArtificialInvaders.exe"
        self._train_mode = True
        self._env = UnityEnvironment(file_name=self._env_name, worker_id=worker_id, seed=1)
        self._default_brain = self._env.brain_names[0]
        self._brain = self._env.brains[self._default_brain]
        self._steps = 0
        self._episodes = 0
        self._game_finished = False
        self._state = None
        self._episode_reward = 0
        self._episode_start_time = time.time()
        atexit.register(self.close)

    def init_game(self):
        self._state = self._env.reset(train_mode=self._train_mode)[self._default_brain]
        # print("Agent state looks like: \n{}".format(self._state.vector_observations[0]))
    
    def close(self):
        self._env.close()

    def get_num_of_actions(self):
        return self._brain.vector_action_space_size[0]
    
    def get_num_of_observations(self):
        observations = self.get_observations()
        shape = observations.shape
        return shape[0]

    def new_episode(self, env_reset_parameters=None):
        self._episodes += 1
        self._episode_start_time = time.time()
        # print("New Episode: {}".format(self._episodes))
        self._state = self._env.reset(train_mode=self._train_mode, config=env_reset_parameters)[self._default_brain]
        self._game_finished = False
        self._steps = 0
        self._episode_reward = 0

    def is_episode_finished(self):
        return self._game_finished

    def make_action(self, action):
        self._steps += 1
        self._state = self._env.step(action)[self._default_brain]
        reward = self._state.rewards[0]
        self._episode_reward += reward
        done = self._state.local_done[0]
        if done == True:
            self._game_finished = True
            # print("Episode {} reward: {:{width}.{prec}f}. Length: {:{width}.{prec}f}s".format(self._episodes, self._episode_reward, (time.time() - self._episode_start_time), width=4, prec=2))

        # print("Action number: {} / {} : Balls: {}".format(self._steps, self._max_steps_per_episode, my_robot_controller.amount_of_balls()))
        return reward
    
    def get_observations(self):
        return self._state.vector_observations[0]
  
  