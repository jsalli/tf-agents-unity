import os

import numpy as np
from tf_agents.environments import py_environment, utils
from tf_agents.specs import array_spec
from tf_agents.trajectories import time_step
from unity_game import UnityGame
import json
import random

class UnityEnvironment(py_environment.PyEnvironment):
    def __init__(self, worker_id, shared_training_status):
        super().__init__()
        self._id = worker_id
        self._shared_training_status = shared_training_status
        worker_id = worker_id if worker_id is not None else random.randint(0,1000)
        self._game = self.configure_ai(worker_id)
        self._num_actions = self._game.get_num_of_actions()
        self._num_observations = self._game.get_num_of_observations()
        self._episode = 0

        self._action_spec = array_spec.BoundedArraySpec(shape=(), dtype=np.int32, minimum=0, maximum=self._num_actions - 1, name='action')
        self._observation_spec = array_spec.BoundedArraySpec(shape=(self._num_observations,), dtype=np.float64, minimum=0, maximum=1, name='observation')
        
        with open('unity_reset_params.json', 'r') as file:
            json_string = file.read()
            self._unity_scene_reset_parameters = json.loads(json_string)
        
        self._current_level = -1
        self._max_level = len(self._unity_scene_reset_parameters["thresholds"])

    # @staticmethod
    def configure_ai(self, worker_id):
        game = UnityGame(worker_id)
        game.init_game()
        return game


    def action_spec(self):
        return self._action_spec


    def observation_spec(self):
        return self._observation_spec


    def _reset(self):
        # print("_reset==== NumberOfEpisodes {}".format(self._shared_training_status["NumberOfEpisodes"]))
        # print("_reset==== EnvironmentSteps {}".format(self._shared_training_status["EnvironmentSteps"]))
        # print("_reset==== AverageReturn {}".format(self._shared_training_status["AverageReturn"]))
        # print("_reset==== AverageEpisodeLength {}".format(self._shared_training_status["AverageEpisodeLength"]))

        reset_params = None
        if (
            self._unity_scene_reset_parameters is not None and
            self._unity_scene_reset_parameters["measure"] == "reward"):
            if (
                self._shared_training_status["AverageReturn"] > self._unity_scene_reset_parameters["thresholds"][self._current_level] or
                self._current_level == -1):
                if self._current_level < self._max_level:
                    self._current_level += 1
                    print("### Loading new level: {}".format(self._current_level + 1))
                    reset_params = {}
                    for key in self._unity_scene_reset_parameters["parameters"].keys():
                        reset_params[key] = self._unity_scene_reset_parameters["parameters"][key][self._current_level]
                    print("### Parameters are: {}".format(reset_params))

        self._game.new_episode(reset_params)
        self._episode += 1
        return time_step.restart(self._game.get_observations())


    def _step(self, action):
        if self._game.is_episode_finished():
          # The last action ended the episode. Ignore the current action and start a new episode.
          return self.reset()

        # execute action and receive reward
        reward = self._game.make_action(np.array([action], dtype=np.int32))

        # return transition depending on game state
        if self._game.is_episode_finished():
          return time_step.termination(self._game.get_observations(), reward)
        else:
          return time_step.transition(self._game.get_observations(), reward)


    def close(self):
        if self._game is not None:
            self._game.close()