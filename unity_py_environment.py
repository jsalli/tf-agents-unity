"""Treat multiple non-batch environments as a single batch environment."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import gin
import numpy as np
import tensorflow as tf

from tf_agents.environments import py_environment
from tf_agents.utils import nest_utils


@gin.configurable
class UnityPyEnvironment(py_environment.PyEnvironment):
  """Batch together multiple Unity environments and act as a single batch.
  """

  def __init__(self, env):
    self._env = env()
    self._num_envs = self._env.batch_size()
    self._action_spec = self._env.action_spec()
    self._observation_spec = self._env.observation_spec()
    self._time_step_spec = self._env.time_step_spec()
    super(UnityPyEnvironment, self).__init__()

  @property
  def batched(self):
    return True

  @property
  def batch_size(self):
    return self._num_envs

  @property
  def envs(self):
    return [self._env]

  def observation_spec(self):
    return self._observation_spec

  def action_spec(self):
    return self._action_spec

  def time_step_spec(self):
    return self._time_step_spec

  def _reset(self):
    time_steps = self._env.reset()
    temp = stack_time_steps(time_steps)
    return temp

  def _step(self, actions):
    unstacked_actions = unstack_actions(actions)
    if len(unstacked_actions) != self.batch_size:
      raise ValueError(
        "Primary dimension of action items does not match "
        "batch size: %d vs. %d" % (len(unstacked_actions), self.batch_size))
    time_steps = self._env.step(unstacked_actions)
    return stack_time_steps(time_steps)


  def close(self):
    """Send close messages to the external process and join them."""
    self._env.close()


# TODO(b/124447001): Factor these helper functions out into common utils.
def stack_time_steps(time_steps):
  """Given a list of TimeStep, combine to one with a batch dimension."""
  return fast_map_structure(lambda *arrays: np.stack(arrays), *time_steps)


def unstack_actions(batched_actions):
  """Returns a list of actions from potentially nested batch of actions."""
  flattened_actions = tf.nest.flatten(batched_actions)
  unstacked_actions = [
      tf.nest.pack_sequence_as(batched_actions, actions)
      for actions in zip(*flattened_actions)
  ]
  return unstacked_actions


def fast_map_structure(func, *structure):
  """List tf.nest.map_structure, but skipping the slow assert_same_structure."""
  flat_structure = [tf.nest.flatten(s) for s in structure]
  entries = zip(*flat_structure)
  return tf.nest.pack_sequence_as(structure[0], [func(*x) for x in entries])