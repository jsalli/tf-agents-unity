### Setup a virtual environment

#### Copy "ml-agents-envs"-folder
Copy "ml-agents-envs"-folder to this projects root-folder from Unity3D ml agents repo: https://github.com/Unity-Technologies/ml-agents


### Build the Unity Artificial invader project
Remember to have only one arena active in the build. Set the other 31-arenas inactive for this build.
Give the build the name "artificial_invaders_1_area". The extension in Linux will be ".x86_64"
Copy the build to this projects env-folder.

### Create Python virtual environment if you want
virtualenv ai_env
source ai_env/bin/activate

### Install Python packages
pip install -r requirements.txt


python ai_tf_agent.py